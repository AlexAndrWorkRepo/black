﻿using BlackJack.Models.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BlackJack.DataAccess
{
    public class BlackJackContext : IdentityDbContext<ApplicationUser>
    {
        public BlackJackContext(DbContextOptions<BlackJackContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<UserGamePlayer>()
                .HasIndex(p => new { p.PlayerId, p.GameId })
                .IsUnique(true);
            modelBuilder.Entity<BotGamePlayer>()
                .HasIndex(p => new { p.PlayerId, p.GameId })
                .IsUnique(true);
        }

        public DbSet<Game> Games { get; set; }
        public DbSet<UserPlayer> UserPlayers { get; set; }
        public DbSet<BotPlayer> BotPlayers { get; set; }
        public DbSet<UserGamePlayer> UserGamePlayers { get; set; }
        public DbSet<BotGamePlayer> BotGamePlayers { get; set; }
        public DbSet<BotCard> BotCards { get; set; }
        public DbSet<UserCard> UserCards { get; set; }
    }
}
