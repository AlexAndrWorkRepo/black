﻿using BlackJack.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJack.DataAccess.Repositories.Interfaces
{
    public interface IBotCardRepository : IBaseRepository<BotCard>
    {
        Task<List<BotCard>> GetAllByGameId(long gameId);
    }
}
