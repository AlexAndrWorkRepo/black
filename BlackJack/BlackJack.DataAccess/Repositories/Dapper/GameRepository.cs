﻿using BlackJack.DataAccess.Repositories.Interfaces;
using BlackJack.Models.Entities;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace BlackJack.DataAccess.Repositories.Dapper
{
    public class GameRepository : BaseRepository<Game>, IGameRepository
    {
        public GameRepository(IDbConnection connection) : base(connection)
        {
        }

        public async Task<List<Game>> GetAllGamesOfUser(string userId)
        {
            var sql = @"select A.* from Games as A
	                        join UserGamePlayers as B on A.Id = B.GameId
	                        join UserPlayers as C on B.PlayerId = C.Id
	                        where C.UserId = @userId";
            var result = await _connection.QueryAsync<Game>(sql, new { userId });
            return result.ToList();
        }

        public async Task<int> GetGamesCountOfUser(string userId)
        {
            var sql = @"select count(*) from Games as A
	                        join UserGamePlayers as B on A.Id = B.GameId
	                        join UserPlayers as C on B.PlayerId = C.Id
	                        where C.UserId = @userId";
            var result = await _connection.QuerySingleAsync<int>(sql, new { userId });
            return result;
        }

        public async Task<List<Game>> GetGamesOfUserOnPage(string userId, int pageNumber, int pageSize)
        {
            var sql = @"select A.* from Games as A
                            join UserGamePlayers as B 
                                on A.Id = B.GameId
							join UserPlayers as C
								on B.PlayerId = C.Id
                            where C.UserId = @userId
                            order by A.Id
                            offset (@skip) row fetch next (@take) rows only";

            var result = await _connection.QueryAsync<Game>(sql, new { userId, skip = (pageNumber - 1) * pageSize, take = pageSize });
            return result.ToList();
        }

        public async Task<Game> GetNotFinishedGame(long playerId)
        {
            var sql = @"select A.* from Games as A
                            join UserGamePlayers as B on A.Id = B.GameId
                            where B.PlayerId = @playerId and A.Finished = 0";
            var result = await _connection.QueryFirstOrDefaultAsync<Game>(sql, new { playerId });
            return result;
        }

        public async Task<Game> GetUserGameById(string userId, long gameId)
        {
            var sql = @"select A.* from Games as A
	                        join UserGamePlayers as B on A.Id = B.GameId
	                        join UserPlayers as C on B.PlayerId = C.Id
	                        where C.UserId = @userId and A.Id = @gameId";
            var result = await _connection.QueryFirstOrDefaultAsync<Game>(sql, new { gameId, userId });
            return result;
        }

        public async Task<bool> HasNotFinishedGame(long playerId)
        {
            var sql = @"select count(*) from Games as A
                            join UserGamePlayers as B on A.Id = B.GameId
                            where B.PlayerId = @playerId and A.Finished = 0";
            var result = await _connection.QueryFirstOrDefaultAsync<int>(sql, new { playerId });
            return result != 0;
        }
    }
}
