﻿using BlackJack.DataAccess.Repositories.Interfaces;
using BlackJack.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackJack.DataAccess.Repositories.EntityFramework
{
    public class BotCardRepository : BaseRepository<BotCard>, IBotCardRepository
    {
        public BotCardRepository(BlackJackContext context) : base(context)
        {
        }

        public async Task<List<BotCard>> GetAllByGameId(long gameId)
        {
            var result = await _context.BotCards
                .Join(_context.BotGamePlayers, c => c.GamePlayerId, gp => gp.Id, (c, gp) => new { Card = c, gp.GameId })
                .Where(p => p.GameId == gameId)
                .Select(p => p.Card)
                .ToListAsync();
            return result;
        }
    }
}
