﻿using BlackJack.DataAccess.Repositories.Interfaces;
using BlackJack.Models.Entities;
using BlackJack.Models.Enums;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackJack.DataAccess.Repositories.EntityFramework
{
    public class BotPlayerRepository : BaseRepository<BotPlayer>, IBotPlayerRepository
    {
        public BotPlayerRepository(BlackJackContext context) : base(context)
        {
        }

        public async Task<List<BotPlayer>> GetBots(int count)
        {
            var result = await _context.BotPlayers
                .Where(b => b.Type == BotType.Player)
                .Take(count)
                .ToListAsync();
            return result;
        }

        public async Task<BotPlayer> GetDealer()
        {
            var result = await _context.BotPlayers
                .Where(b => b.Type == BotType.Dealer)
                .FirstOrDefaultAsync();
            return result;
        }
    }
}
