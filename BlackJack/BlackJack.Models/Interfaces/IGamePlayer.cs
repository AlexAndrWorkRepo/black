﻿using BlackJack.Models.Enums;

namespace BlackJack.Models.Interfaces
{
    public interface IGamePlayer : IBaseEntity
    {
        long GameId { get; set; }
        long PlayerId { get; set; }
        int Points { get; set; }
        PlayerStatusType Status { get; set; }
    }
}
