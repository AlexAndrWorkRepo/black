﻿using System;

namespace BlackJack.Models.Interfaces
{
    public interface IBaseEntity
    {
        long Id { get; set; }
        DateTime CreationDate { get; set; }
    }
}
