﻿using BlackJack.Models.Enums;

namespace BlackJack.Models.Interfaces
{
    public interface ICard : IBaseEntity
    {
        long GamePlayerId { get; set; }
        CardNameType CardName { get; set; }
        CardSuitType CardSuit { get; set; }
    }
}
