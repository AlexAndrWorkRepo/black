﻿namespace BlackJack.Models.Interfaces
{
    public interface IPlayer : IBaseEntity
    {
        string Name { get; set; }
    }
}
