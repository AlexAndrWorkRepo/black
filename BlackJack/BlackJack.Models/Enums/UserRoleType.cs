﻿namespace BlackJack.Models.Enums
{
    public enum UserRoleType
    {
        User = 0,
        Admin = 1
    }
}
