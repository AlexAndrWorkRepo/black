﻿namespace BlackJack.Models.Enums
{
    public enum BotType
    {
        Dealer = 0,
        Player = 1,
    }
}
