﻿using BlackJack.Models.Enums;
using BlackJack.Models.Interfaces;
using Dapper.Contrib.Extensions;

namespace BlackJack.Models.Entities
{
    public class UserGamePlayer : BaseEntity, IGamePlayer
    {
        public long GameId { get; set; }
        public long PlayerId { get; set; }
        public PlayerStatusType Status { get; set; }
        public int Points { get; set; }

        [Write(false)]
        public virtual UserPlayer Player { get; set; }
        [Write(false)]
        public virtual Game Game { get; set; }
    }
}
