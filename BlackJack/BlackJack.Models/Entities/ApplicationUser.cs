﻿using BlackJack.Models.Enums;
using Microsoft.AspNetCore.Identity;

namespace BlackJack.Models.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public UserRoleType Role { get; set; }
    }
}
