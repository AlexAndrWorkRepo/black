﻿using BlackJack.Models.Enums;
using BlackJack.Models.Interfaces;

namespace BlackJack.Models.Entities
{
    public class BotPlayer : BaseEntity, IPlayer
    {
        public string Name { get; set; }
        public BotType Type { get; set; }
    }
}
