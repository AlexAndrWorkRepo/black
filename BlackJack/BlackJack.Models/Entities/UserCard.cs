﻿using BlackJack.Models.Enums;
using BlackJack.Models.Interfaces;
using Dapper.Contrib.Extensions;

namespace BlackJack.Models.Entities
{
    public class UserCard : BaseEntity, ICard
    {
        public long GamePlayerId { get; set; }
        public CardNameType CardName { get; set; }
        public CardSuitType CardSuit { get; set; }

        [Write(false)]
        public virtual UserGamePlayer GamePlayer { get; set; }
    }
}
