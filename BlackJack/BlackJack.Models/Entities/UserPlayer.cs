﻿using BlackJack.Models.Interfaces;
using Dapper.Contrib.Extensions;

namespace BlackJack.Models.Entities
{
    public class UserPlayer : BaseEntity, IPlayer
    {
        public string Name { get; set; }
        public string UserId { get; set; }

        [Write(false)]
        public virtual ApplicationUser User { get; set; }
    }
}
