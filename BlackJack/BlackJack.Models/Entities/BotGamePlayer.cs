﻿using BlackJack.Models.Enums;
using BlackJack.Models.Interfaces;
using Dapper.Contrib.Extensions;

namespace BlackJack.Models.Entities
{
    public class BotGamePlayer : BaseEntity, IGamePlayer
    {
        public long GameId { get; set; }
        public long PlayerId { get; set; }
        public int Points { get; set; }
        public PlayerStatusType Status { get; set; }

        [Write(false)]
        public virtual Game Game { get; set; }
        [Write(false)]
        public virtual BotPlayer Player { get; set; }
    }
}
