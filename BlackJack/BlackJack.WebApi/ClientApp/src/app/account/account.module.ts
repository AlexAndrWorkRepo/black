import { NgModule } from '@angular/core';
import { AccountRoutingModule } from './account-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AccountComponent } from './account.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [AccountComponent],
  imports: [
    AccountRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers: [
  ]
})
export class AccountModule { }
