import { NgModule } from '@angular/core';
import { RegisterComponent } from './register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NoAuthGuard } from 'src/app/shared/guards/no-auth.guard';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [RegisterComponent],
  imports: [
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([{ path: '', component: RegisterComponent, canActivate: [NoAuthGuard] }]),
  ],
  bootstrap: []
})
export class RegisterModule { }
