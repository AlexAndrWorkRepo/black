import { Component, OnInit, Injector } from '@angular/core';
import { BaseComponent } from '../shared/components/base-component';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent extends BaseComponent {

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
  }

}
