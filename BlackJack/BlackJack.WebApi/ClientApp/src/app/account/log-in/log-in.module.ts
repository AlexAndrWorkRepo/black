import { NgModule } from '@angular/core';
import { LogInComponent } from './log-in.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NoAuthGuard } from 'src/app/shared/guards/no-auth.guard';
import { SharedModule } from 'src/app/shared/shared.module';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  declarations: [LogInComponent],
  imports: [
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([{ path: '', component: LogInComponent, canActivate: [NoAuthGuard] }]),
  ]
})
export class LogInModule { }
