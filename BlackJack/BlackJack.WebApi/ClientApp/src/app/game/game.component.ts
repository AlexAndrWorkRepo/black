import { Component, OnInit, Injector } from '@angular/core';
import { BaseComponent } from '../shared/components/base-component';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent extends BaseComponent {

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
  }

}
