import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PlayComponent } from './play.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [PlayComponent],
  imports: [
    SharedModule,
    RouterModule.forChild([{ path: '', component: PlayComponent }]),
  ]
})
export class PlayModule { }
