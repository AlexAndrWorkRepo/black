import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StartGuard } from '../shared/guards/start.guard';
import { PlayGuard } from '../shared/guards/play.guard';
import { GameComponent } from './game.component';

const routes: Routes = [
  {
    path: '', component: GameComponent, children: [
      { path: '', redirectTo: 'start' },
      { path: 'start', loadChildren: './start/start.module#StartModule', canActivate: [StartGuard] },
      { path: 'play', loadChildren: './play/play.module#PlayModule', canActivate: [PlayGuard] }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GameRoutingModule { }
