import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FullHistoryComponent } from './full-history.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [FullHistoryComponent],
  imports: [
    SharedModule,
    RouterModule.forChild([{ path: '', component: FullHistoryComponent }]),
  ]
})
export class FullHistoryModule { }
