import { Component, OnInit, Injector } from '@angular/core';
import { BaseComponent } from '../shared/components/base-component';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent extends BaseComponent {

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
  }
}
