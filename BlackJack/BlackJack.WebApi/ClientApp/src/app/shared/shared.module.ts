import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { PlayingCardComponent } from './components/cards/playing-card/playing-card.component';
import { PlayingCardsComponent } from './components/cards/playing-cards/playing-cards.component';
import { AuthNavMenuComponent } from './components/nav-menu/auth-nav-menu/auth-nav-menu.component';
import { NoAuthNavMenuComponent } from './components/nav-menu/no-auth-nav-menu/no-auth-nav-menu.component';
import { HomeNavMenuComponent } from './components/nav-menu/home-nav-menu/home-nav-menu.component';
import { AccountService } from './services/account.service';
import { HttpHeadersInterceptor } from './interceptors/http-headers.interceptor';
import { FormsModule } from '@angular/forms';
import { HttpErrorInterceptor } from './interceptors/http-error.interceptor';
import { PaginationComponent } from './components/pagination/pagination.component';

@NgModule({
  declarations: [
    PaginationComponent,
    PlayingCardComponent,
    PlayingCardsComponent,
    AuthNavMenuComponent,
    NoAuthNavMenuComponent,
    HomeNavMenuComponent,
    PaginationComponent
  ],
  imports: [
    FormsModule,
    HttpClientModule,
    CommonModule,
    RouterModule,
    ToastrModule.forRoot()
  ],
  exports: [
    FormsModule,
    CommonModule,
    HttpClientModule,
    RouterModule,
    ToastrModule,
    PaginationComponent,
    PlayingCardComponent,
    PlayingCardsComponent,
    HomeNavMenuComponent,
    AuthNavMenuComponent,
    NoAuthNavMenuComponent
  ],
  providers: [
    AccountService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpHeadersInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    }
  ]
})
export class SharedModule { }
