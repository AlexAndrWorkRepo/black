import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { StartGameView } from '../models/game/start-game-view';
import { InsuranceGameView } from '../models/game/insurance-game-view';
import { environment } from 'src/environments/environment';
import { StateGameResponseView } from '../models/game/state-game-response-view';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  constructor(private http: HttpClient) { }

  startGame(model: StartGameView): Observable<boolean> {
    return this.http.post<boolean>(`${environment.apiUrl}/game/start`, model);
  }
  hit(): Observable<StateGameResponseView> {
    return this.http.get<StateGameResponseView>(`${environment.apiUrl}/game/hit`);
  }
  stand(): Observable<StateGameResponseView> {
    return this.http.get<StateGameResponseView>(`${environment.apiUrl}/game/stand`);
  }
  insurance(model: InsuranceGameView): Observable<StateGameResponseView> {
    return this.http.post<StateGameResponseView>(`${environment.apiUrl}/game/insurance`, model);
  }
  load(): Observable<StateGameResponseView> {
    return this.http.get<StateGameResponseView>(`${environment.apiUrl}/game/load`);
  }
  hasNotFinished(): Observable<boolean> {
    return this.http.get<boolean>(`${environment.apiUrl}/game/hasnotfinished`);
  }
}
