import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageDataService {

  constructor() { }

  getValue<T>(key: string): T {
    const result = JSON.parse(localStorage.getItem(key)) as T;
    return result;
  }
  setValue<T>(key: string, value: T) {
    const strValue = JSON.stringify(value);
    localStorage.setItem(key, strValue);
  }
  removeValue(key: string) {
    localStorage.removeItem(key);
  }
}
