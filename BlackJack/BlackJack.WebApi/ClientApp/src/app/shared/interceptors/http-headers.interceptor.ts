import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { localStorageDataKeysConfig } from '../configs/local-storage-data-keys.config';
import { LocalStorageDataService } from '../services/local-storage-data.service';
import { Observable } from 'rxjs';
import { LogInAccountResponseView } from '../models/account/log-in-account-response-view';

@Injectable({ providedIn: 'root' })
export class HttpHeadersInterceptor implements HttpInterceptor {
  constructor(private storageService: LocalStorageDataService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const userInfo = this.storageService.getValue<LogInAccountResponseView>(localStorageDataKeysConfig.user);
    if (userInfo != null) {
      const request = req.clone({
        setHeaders: {
          Authorization: `Bearer ${userInfo.token}`,
          'Content-Type': 'application/json'
        }
      });
      return next.handle(request);
    }
    return next.handle(req);
  }
}
