export class GetHistoryResponseView {
  public games: GameGetHistoryResponseViewItem[];
  public pagination: PaginationGetHistoryResponseViewItem;
  constructor() {
    this.games = [];
  }
}
export class GameGetHistoryResponseViewItem {
  public id: number;
  public creationDate: Date;
  public finished: boolean;
  public playerName: string;
}
export class PaginationGetHistoryResponseViewItem {
  public page: number;
  public totalPagesCount: number;
}
