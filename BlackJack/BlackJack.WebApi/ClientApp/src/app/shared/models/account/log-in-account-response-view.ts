export class LogInAccountResponseView {
  public token: string;
  public username: string;
}
