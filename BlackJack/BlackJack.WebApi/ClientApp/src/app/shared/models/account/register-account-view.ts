export class RegisterAccountView {
  public email: string;
  public userName: string;
  public password: string;
  public confirmPassword: string;

  constructor() {
    this.email = "";
    this.userName = "";
    this.password = "";
    this.confirmPassword = "";
  }
}
