import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/shared/services/account.service';

@Component({
  selector: 'app-home-nav-menu',
  templateUrl: './home-nav-menu.component.html',
  styleUrls: ['./home-nav-menu.component.css']
})
export class HomeNavMenuComponent implements OnInit {

  isAuthenticated: boolean;
  constructor(private accountService: AccountService) { }

  ngOnInit() {
    this.isAuthenticated = this.accountService.isAuthenticated();
  }
}
