import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-no-auth-nav-menu',
  templateUrl: './no-auth-nav-menu.component.html',
  styleUrls: ['./no-auth-nav-menu.component.css']
})
export class NoAuthNavMenuComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
