import { ToastrService } from 'ngx-toastr';
import { OnInit, Injector, Component } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { map, catchError, finalize } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';

export class BaseComponent implements OnInit {

  private toastr: ToastrService;
  public loading: boolean;

  ngOnInit(): void {
  }
  constructor(injector: Injector) {
    this.toastr = injector.get(ToastrService);
  }
  public showSuccess(message: string) {
    this.toastr.success(message, 'Success!');
    this.toastr.show();
  }

  public showError(message: string) {
    this.toastr.error(message, 'Oops!');
  }

  public showWarning(message: string) {
    this.toastr.warning(message, 'Alert!');
  }

  public showInfo(message: string) {
    this.toastr.info(message);
  }

  public executeObservable<T>(func: Observable<T>): Observable<{} | T> {
    this.loading = true;
    return func.pipe(
      map(
        (success: T) => {
          return success;
        }
      ),
      catchError(error => {
        if (error instanceof HttpErrorResponse && typeof error.error === "string") {
          this.showError(error.error);
        }
        if (error instanceof HttpErrorResponse && typeof error.error !== "string") {
          this.showError(error.message);
        }
        return null;
      }),
      finalize(() => {
        this.loading = false;
      })
    );
  }
}
