import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { GameService } from '../services/game.service';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root'})
export class StartGuard implements CanActivate {
  constructor(private gameService: GameService, private router: Router) { }

  canActivate(): Observable<boolean> {
    return this.gameService.hasNotFinished().pipe(
      map((data: boolean) => {
        if (data) {
          this.router.navigateByUrl('/game/play');
          return false;
        }
        return true;
      })
    );
  }
}
