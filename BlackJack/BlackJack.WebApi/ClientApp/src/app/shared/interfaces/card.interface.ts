import { CardSuitViewType } from '../models/enums/card-suit-view-type';
import { CardNameViewType } from '../models/enums/card-name-view-type';

export interface Card {
  suit: CardSuitViewType;
  name: CardNameViewType;
}
