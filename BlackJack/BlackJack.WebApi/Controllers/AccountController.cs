﻿using System.Threading.Tasks;
using BlackJack.BusinessLogic.Services.Interfaces;
using BlackJack.ViewModels.Account;
using Microsoft.AspNetCore.Mvc;

namespace BlackJack.WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    public class AccountController : BaseController
    {
        private readonly IAccountService _accountService;
        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }
        [HttpPost]
        public async Task<IActionResult> LogIn([FromBody]LogInAccountView model)
        {
            var result = await _accountService.LogIn(model);
            return Ok(result);
        }
        [HttpPost]
        public async Task<IActionResult> Register([FromBody]RegisterAccountView model)
        {
            var result = await _accountService.Register(model);
            return Ok(result);
        }
    }
}
