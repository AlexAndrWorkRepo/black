﻿using System.Threading.Tasks;
using BlackJack.BusinessLogic.Services.Interfaces;
using BlackJack.ViewModels.Game;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BlackJack.WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [Authorize]
    public class GameController : BaseController
    {
        public IGamePlayService _gamePlayService;

        public GameController(IGamePlayService gamePlayService)
        {
            _gamePlayService = gamePlayService;
        }

        [HttpPost]
        public async Task<IActionResult> Start([FromBody]StartGameView model)
        {
            /*var result =*/ await _gamePlayService.Start(UserId, model);
            return Ok(true/*result*/);
        }

        [HttpGet]
        public async Task<IActionResult> Hit()
        {
            var result = await _gamePlayService.Hit(UserId);
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> Stand()
        {
            var result = await _gamePlayService.Stand(UserId);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Insurance([FromBody]InsuranceGameView model)
        {
            var result = await _gamePlayService.Insurance(UserId, model);
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> Load()
        {
            var result = await _gamePlayService.Load(UserId);
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> HasNotFinished()
        {
            var result = await _gamePlayService.HasNotFinishedGame(UserId);
            return Ok(result);
        }
    }
}