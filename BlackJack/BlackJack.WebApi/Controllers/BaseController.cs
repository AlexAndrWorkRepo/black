﻿using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;

namespace BlackJack.WebApi.Controllers
{
    public class BaseController : Controller
    {
        protected string UserId { get => User.FindFirst(ClaimTypes.NameIdentifier)?.Value ?? string.Empty; }
    }
}