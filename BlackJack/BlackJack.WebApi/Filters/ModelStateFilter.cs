﻿using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace BlackJack.WebApi.Filters
{
    public class ModelStateFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                var firstError = context.ModelState.Values.FirstOrDefault().Errors.FirstOrDefault().ErrorMessage;
                context.Result = new BadRequestObjectResult(firstError);
            }
        }
    }
}
