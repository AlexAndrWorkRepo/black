﻿using BlackJack.Models.Entities;

namespace BlackJack.BusinessLogic.Providers.Interfaces
{
    public interface IJwtProvider
    {
        string GenerateToken(ApplicationUser user);
    }
}
