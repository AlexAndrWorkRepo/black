﻿namespace BlackJack.BusinessLogic.Options
{
    public class DbOption
    {
        public string ConnectionStringName { get; set; }
        public string OrmName { get; set; }
    }
}
