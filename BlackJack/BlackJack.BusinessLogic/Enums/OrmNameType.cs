﻿namespace BlackJack.BusinessLogic.Enums
{
    public enum OrmNameType
    {
        Dapper = 0,
        EntityFramework = 1
    }
}
