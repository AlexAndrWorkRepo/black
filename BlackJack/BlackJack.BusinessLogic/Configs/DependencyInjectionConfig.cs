﻿using BlackJack.BusinessLogic.Enums;
using BlackJack.BusinessLogic.Options;
using BlackJack.DataAccess;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Data;
using System.Data.SqlClient;

namespace BlackJack.BusinessLogic.Configs
{


    public static class DependencyInjectionConfig
    {
        public static void ConfigureDependencyInjections(this IServiceCollection services, IConfiguration configuration)
        {
            DbConfigs dbOptions = null;
            using (var serviceProvider = services.BuildServiceProvider())
            {
                dbOptions = serviceProvider.GetService<DbConfigs>() ?? throw new ApplicationException("Db options is null.");
            }

            services.Scan(scanner =>
            {
                scanner.FromAssemblyOf<DbConfigs>()
                    .AddClasses(c => c.InExactNamespaces("BlackJack.BusinessLogic.Services", "BlackJack.BusinessLogic.Helpers"))
                    .AsImplementedInterfaces()
                    .WithTransientLifetime();
            });
            if (dbOptions.OrmNameType == OrmNameType.Dapper)
            {
                services.Scan(scanner =>
                {
                    scanner.FromAssemblyOf<BlackJackContext>()
                       .AddClasses(c => c.InExactNamespaces("BlackJack.DataAccess.Repositories.Dapper"))
                       .AsImplementedInterfaces()
                       .WithScopedLifetime();
                });
                services.AddTransient<IDbConnection>(c => new SqlConnection(dbOptions.ConnectionString));
            }
            if (dbOptions.OrmNameType == OrmNameType.EntityFramework)
            {
                services.Scan(scanner =>
                {
                    scanner.FromAssemblyOf<BlackJackContext>()
                        .AddClasses(c => c.InExactNamespaces("BlackJack.DataAccess.Repositories.EntityFramework"))
                        .AsImplementedInterfaces()
                        .WithScopedLifetime();
                });
            }
        }

    }
}
