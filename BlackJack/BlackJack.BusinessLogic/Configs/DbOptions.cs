﻿using BlackJack.BusinessLogic.Enums;
using BlackJack.BusinessLogic.Options;
using Microsoft.Extensions.Configuration;
using System;

namespace BlackJack.BusinessLogic.Configs
{
    public class DbConfigs
    {
        public string ConnectionString { get; }
        public OrmNameType OrmNameType { get; }

        public DbConfigs(IConfiguration configuration)
        {
            var dbOption = configuration.GetSection("DbOption").Get<DbOption>() ?? throw new ApplicationException("Db option is null.");

            ConnectionString = configuration.GetConnectionString(dbOption.ConnectionStringName);

            if (string.IsNullOrEmpty(ConnectionString))
            {
                throw new ApplicationException("Connection string do not find.");
            }
            if (!Enum.TryParse(dbOption.OrmName, out OrmNameType ormNameType))
            {
                throw new ApplicationException("Unknown type of ORM.");
            }
            OrmNameType = ormNameType;
        }

    }
}
