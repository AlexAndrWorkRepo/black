﻿using System;
using System.Collections.Generic;

namespace BlackJack.ViewModels.History
{
    public class GetHistoryResponseView
    {
        public List<GameGetHistoryResponseViewItem> Games { get; set; }
        public PaginationGetHistoryResponseViewItem Pagination { get; set; }

        public GetHistoryResponseView()
        {
            Games = new List<GameGetHistoryResponseViewItem>();
        }
    }

    public class GameGetHistoryResponseViewItem
    {
        public long Id { get; set; }
        public DateTime CreationDate { get; set; }
        public bool Finished { get; set; }
        public string PlayerName { get; set; }
    }

    public class PaginationGetHistoryResponseViewItem
    {
        public int Page { get; set; }
        public int TotalPagesCount { get; set; }
    }
}
