﻿using BlackJack.ViewModels.Enums;
using System.Collections.Generic;

namespace BlackJack.ViewModels.History
{
    public class DetailsHistoryResponseView
    {
        public long GameId { get; set; }
        public bool Finished { get; set; }
        public bool Insuranced { get; set; }
        public List<PlayerDetailsHistoryResponseViewItem> Players { get; set; }

        public DetailsHistoryResponseView()
        {
            Players = new List<PlayerDetailsHistoryResponseViewItem>();
        }
    }

    public class CardDetailsHistoryResponseViewItem
    {
        public CardNameViewType Name { get; set; }
        public CardSuitViewType Suit { get; set; }
    }

    public class PlayerDetailsHistoryResponseViewItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public PlayerStatusViewType Status { get; set; }
        public int Points { get; set; }
        public RoleViewType Role { get; set; }
        public List<CardDetailsHistoryResponseViewItem> Cards { get; set; }

        public PlayerDetailsHistoryResponseViewItem()
        {
            Cards = new List<CardDetailsHistoryResponseViewItem>();
        }
    }
}
