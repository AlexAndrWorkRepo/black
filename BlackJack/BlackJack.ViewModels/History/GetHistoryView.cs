﻿using System.ComponentModel.DataAnnotations;

namespace BlackJack.ViewModels.History
{
    public class GetHistoryView
    {
        [Required]
        public int Page { get; set; }
    }
}
