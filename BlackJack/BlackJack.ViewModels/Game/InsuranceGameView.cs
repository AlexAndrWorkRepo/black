﻿using System.ComponentModel.DataAnnotations;

namespace BlackJack.ViewModels.Game
{
    public class InsuranceGameView
    {
        [Required]
        public bool Insurance { get; set; }
    }
}
