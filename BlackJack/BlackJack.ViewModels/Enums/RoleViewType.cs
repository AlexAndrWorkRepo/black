﻿namespace BlackJack.ViewModels.Enums
{
    public enum RoleViewType
    {
        Dealer = 0,
        Bot = 1,
        Player = 2,
    }
}
