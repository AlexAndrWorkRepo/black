﻿namespace BlackJack.ViewModels.Enums
{
    public enum CardSuitViewType
    {
        Diamonds = 0,
        Hearts = 1,
        Spades = 2,
        Clubs = 3
    }
}
