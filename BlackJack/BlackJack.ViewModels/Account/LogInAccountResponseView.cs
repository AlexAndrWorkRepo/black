﻿namespace BlackJack.ViewModels.Account
{
    public class LogInAccountResponseView
    {
        public string Token { get; set; }
        public string Username { get; set; }
    }
}
