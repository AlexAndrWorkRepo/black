﻿using System.ComponentModel.DataAnnotations;

namespace BlackJack.ViewModels.Account
{
    public class LogInAccountView
    {
        [Required]
        [MinLength(6)]
        public string Username { get; set; }
        [Required]
        [MinLength(8)]
        public string Password { get; set; }
    }
}
