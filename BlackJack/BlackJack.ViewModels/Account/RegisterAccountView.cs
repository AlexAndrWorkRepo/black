﻿using System.ComponentModel.DataAnnotations;

namespace BlackJack.ViewModels.Account
{
    public class RegisterAccountView
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [MinLength(6)]
        public string UserName { get; set; }
        [Required]
        [MinLength(8)]
        public string Password { get; set; }
        [Required]
        [MinLength(8)]
        public string ConfirmPassword { get; set; }
    }
}
