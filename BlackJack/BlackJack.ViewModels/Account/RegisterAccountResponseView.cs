﻿namespace BlackJack.ViewModels.Account
{
    public class RegisterAccountResponseView
    {
        public string Token { get; set; }
        public string Username { get; set; }
    }
}
